//
//  DetailsViewModel.swift
//  NYCSchools
//
//  Created by David Sahagun Mayorga on 9/15/23.
//

import Foundation
import SwiftUI
import Combine

class DetailsViewModel: ObservableObject{
    @Published var items: [SchoolDetail] = []
    
    init(){
        fetchData()
    }
    
    func fetchData(){
        guard let apiURL = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") else{
            //Error handler
            return
        }
        
        URLSession.shared.dataTask(with: apiURL){data, response, error in
            if let error = error{
                print("Error: \(error)")
                return
            }
            
            if let data = data{
                do{
                    let decoder = JSONDecoder()
                    let decodedData = try decoder.decode([SchoolDetail].self, from: data)
                    DispatchQueue.main.async {
                        self.items = decodedData
                    }
                }catch{
                    print("Error decoding data \(error)")
                }
            }
        }.resume()
    }
}
