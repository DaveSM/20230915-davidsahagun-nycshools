//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by David Sahagun Mayorga on 9/15/23.
//

import Foundation
import SwiftUI
import Combine

class SchoolViewModel: ObservableObject{
    @Published var schools: [School] = []
    
    init(){
        fetchData()
    }
    
    func fetchData(){
        guard let apiUrl = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else{
            return //we can handle the if the API Url is not set
        }
        
        let task = URLSession.shared.dataTask(with: apiUrl){ (data, response, error) in
            if let error = error{
                //We can handle a network error here
                print("Error: \(error)")
                return
            }
            
            if let data = data{
                do{
                    let decoder = JSONDecoder()
                    let decodedSchools = try decoder.decode([School].self, from: data)
                    
                    //Update @Published property
                    DispatchQueue.main.async {
                        self.schools = decodedSchools
                    }
                }catch{
                    //Handle decode errors
                    print("Error decoding the JSON: \(error)")
                }
            }
        }
        task.resume()
    }
}
