//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by David Sahagun Mayorga on 9/15/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            Schools()
        }
    }
}
