//
//  SchoolDetailsView.swift
//  NYCSchools
//
//  Created by David Sahagun Mayorga on 9/15/23.
//

import SwiftUI

struct SchoolDetailsView: View {
    @ObservedObject var detailVM = DetailsViewModel()
    
    let selectedSchool: School
    var body: some View {
        VStack{
            Text("Details about the school")
            Spacer()
            VStack(alignment: .leading){
                Text("🏫 \(selectedSchool.school_name!)")
                    .lineLimit(2)
                Text("📞  \(selectedSchool.phone_number!)")
                    .padding(.top)
                if !(selectedSchool.school_email?.isEmpty ?? true){
                    Text("📧 \(selectedSchool.school_email!)")
                        .foregroundColor(Color.blue)
                }
                let actualDetails = detailsCheck(dbn: selectedSchool.dbn!)
                Text("SAT Scores:")
                    .fontWeight(.bold)
                    .padding(.top)
                Text("🧮 Math AVG: \(actualDetails.sat_math_avg_score!)")
                Text("📚 Reading AVG: \(actualDetails.sat_critical_reading_avg_score!)")
                Text("📝 Writing AVG: \(actualDetails.sat_writing_avg_score!)")
                Spacer()
                //If I had more time I would add a map kit in here and use the coordinates to show the school location on a map
            }
        }
    }
    
    func detailsCheck(dbn:String)->SchoolDetail{
        let schoolDetailsData = detailVM.items
        let itExists = schoolDetailsData.contains{$0.dbn == dbn}
        if itExists{
            let detail = schoolDetailsData.first(where: {$0.dbn == dbn})!            
            return detail
        }
        let notFound = SchoolDetail(dbn: "Not Found", school_name: "Not Found", num_of_sat_test_takers: "Not Found", sat_critical_reading_avg_score: "Not Found", sat_math_avg_score: "Not Found", sat_writing_avg_score: "Not Found")
        return notFound
    }
        
    //SET A STRUCT SO IT CAN BE VISIBLE IN THE PREVIEW
    static func priviewWithSelectedSchool() -> some View{
        let selectedSchool = School(dbn: "Test", school_name: "Test Scool Name", boro: "Test Boro", overview_paragraph: "Test overview", school_10th_seats: "Seats", academicopportunities1: "Test academic", academicopportunities2: "Test Oportunities", ell_programs: "Test ell", neighborhood: "Neighbo", building_code: "Building Code", location: "Location", phone_number: "702-123-1232", fax_number: "Fax", school_email: "email", website: "web", subway: "Subway", bus: "bus", grades2018: "grades", finalgrades: "finalGracdes", total_students: "Students", extracurricular_activities: "asd", school_sports: "test", attendance_rate: "asda", pct_stu_enough_variety: "asda", pct_stu_safe: "qasd", school_accessibility_description: "asdasd", directions1: "asdas", requirement1_1: "adasd", requirement2_1: "asdas", requirement3_1: "asdfad", requirement4_1: "adasda", requirement5_1: "asdasd", offer_rate1: "asdasd", program1: "pgra", code1: "cod", interest1: "inters", method1: "met", seats9ge1: "seat9", grade9gefilledflag1: "fill", grade9geapplicants1: "tes", seats9swd1: "asd", grade9swdfilledflag1: "Asdasd", grade9swdapplicants1: "ada", seats101: "asda", admissionspriority11: "asda", admissionspriority21: "asdas", admissionspriority31: "asdasd", grade9geapplicantsperseat1: "aasda", grade9swdapplicantsperseat1: "asdas", primary_address_line_1: "asdas", city: "asda", zip: "asdasd", state_code: "asdasd", latitude: "asdasd", longitude: "asdasd", community_board: "asdasd", council_district: "asdasd", census_tract: "asdasd", bin: "asdasd", bbl: "asdasd", nta: "asdasd", borough: "adssad")
        return SchoolDetailsView(selectedSchool: selectedSchool)
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    
    static var previews: some View {
        //LOADING THE PRVIEW WITH THE DUMMY DATA
        SchoolDetailsView.priviewWithSelectedSchool()
    }
}
