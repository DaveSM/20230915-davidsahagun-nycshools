//
//  Schools.swift
//  NYCSchools
//
//  Created by David Sahagun Mayorga on 9/15/23.
//

import SwiftUI

struct Schools: View {
    @ObservedObject var viewModel = SchoolViewModel()
    var body: some View {
        NavigationView{
            List(viewModel.schools, id: \.dbn){school in
                NavigationLink(destination: SchoolDetailsView(selectedSchool: school)){
                    VStack(alignment: .leading){
                        Text(school.school_name!).font(.headline)
                        Text(school.primary_address_line_1!)
                        Text(school.city!)
                        Text(school.school_email ?? "")
                            .foregroundColor(Color.blue)
                        Text(school.phone_number ?? "")
                            .fontWeight(.light)
                            .foregroundColor(Color.gray)
                    }
                }
            }
        }
        .navigationTitle("Details")
        //If more time I would add a dark mode as well, today I had several calls I couln´t fouced unitl the after noon.
    }
}

struct Schools_Previews: PreviewProvider {
    static var previews: some View {
        Schools()
    }
}
